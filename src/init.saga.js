import { put, call } from 'redux-saga/effects'
import { Alert, BackHandler } from 'react-native'
import axios from 'axios'

//import { } from './utils'
//import config from './environment/config'
//import { createSocketSubscription } from './sockets'
//import store from './store'
// import mockNotifications from './mocks/notifications'

var err = 0

export default function* init({ payload }) {
  try {
    const { navigation } = payload
    yield put({ type: 'SHOW_LOADING' })
    const data1 = yield call(axios.get,'https://mindicador.cl/api')
    yield put({type: 'SET_LISTA_INDICADORES_MAIN', payload: Object.values(data1.data)})
    yield navigation.navigate('Base')
  } catch (error) {
    Alert.alert(
      'Se produjo un error',
      'Intente nuevamente'
    )
  } finally {
    yield put({ type: 'HIDE_LOADING' })
  }
}
