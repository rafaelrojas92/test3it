import _ from 'lodash'
import React from 'react'
import { connect } from 'react-redux'
import { StyleSheet, } from 'react-native'
import { Modal, Portal } from 'react-native-paper'
import { Flow } from 'react-native-animated-spinkit'
import { createStackNavigator } from '@react-navigation/stack'

import { BaseScreens, } from './routes'

const Stack = createStackNavigator()

const Navigation = ({ loading }) => {

  return (
    <>
      <Stack.Navigator
        headerMode='screen'
        initialRouteName='Base'
        screenOptions={{ headerShown: false }}
      >
        {Object.entries({
          ...BaseScreens,
        }).map(([name, component], i) => (
          <Stack.Screen
            key={i.toString()}
            name={name}
            component={component}
          />
        ))}
      </Stack.Navigator>
      <Portal>
        <Modal
          visible={loading}
          dismissable={false}
          style={styles.containerLoading}
        >
          <Flow animating={true} size={100} color='#000080' />
        </Modal>
      </Portal>
    </>
  )
}

const styles = StyleSheet.create({
  connectInfo: {
    top: -5,
    height: 32,
    width: '100%',
  },
  containerLoading: {
    marginBottom: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

const mapStateToProps = state => {
  const {
    Base: {
      loading,
    },
  } = state

  return {
    loading,
  }
}
const mapDispatchToProps = dispatch => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Navigation)
