import { Dimensions, PixelRatio, Platform } from 'react-native'

const { height, width } = Dimensions.get('window')

export const widthPercetageToDP = widthPercent => {
  const elemWidth = parseFloat(widthPercent)

  return PixelRatio.roundToNearestPixel((width * elemWidth) / 100)
}

export const heightPercentageToDP = heightPercent => {
  const elemHeight = parseFloat(heightPercent)

  return PixelRatio.roundToNearestPixel((height * elemHeight) / 100)
}

const { width: screenWidth, height: screenHeight } = Dimensions.get('window')

const guidelineBaseWidth = 320
const guidelineBaseHeight = 480

const scale = size => (screenWidth / guidelineBaseWidth) * size
const verticalScale = size => (screenHeight / guidelineBaseHeight) * size
const moderateScale = (size, factor = 0.5) =>
  size + (scale(size) - size) * factor

const memoSizes = {}

export const widthPercent = px => {
  if (!memoSizes[`${px}-w`]) {
    const _px = PixelRatio.roundToNearestPixel(moderateScale(px))
    memoSizes[`${px}-w`] = px < _px ? px : _px
  }

  return memoSizes[`${px}-w`]
}

export const heightPercent = px => {
  if (!memoSizes[`${px}-h`]) {
    const _px = PixelRatio.roundToNearestPixel(verticalScale(px) - 0.7)
    memoSizes[`${px}-h`] = px < _px ? px : _px
  }

  return memoSizes[`${px}-h`]
}

export const normalizeSize = size => {
  if (!memoSizes[size]) {
    const _size = Math.round(
      PixelRatio.roundToNearestPixel(
        ((screenWidth / guidelineBaseWidth) * size) /
        (PixelRatio.getFontScale() > 1.25 ? PixelRatio.getFontScale() : 1.26),
      ),
    )

    memoSizes[size] = _size
  }

  return memoSizes[size]
}

export const COLORS = {
  primary: '#000080',
  background: '#d9d9d9',
  black: '#000000',
  white: '#ffffff',
  gray: '#8f8f8f'
}

export const SIZES = {
  h1: normalizeSize(25),
  body1: normalizeSize(15),
}

const appTheme = {
  SIZES,
  COLORS,
  normalizeSize,
  widthPercetageToDP,
  heightPercentageToDP,
}

export default appTheme
