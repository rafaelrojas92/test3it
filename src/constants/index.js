import {
    FONTS,
    SIZES,
    COLORS,
    normalizeSize,
    widthPercetageToDP,
    heightPercentageToDP,
  } from './theme'
  
  export {
    COLORS,
    SIZES,
    normalizeSize,
    widthPercetageToDP,
    heightPercentageToDP,
  }
  