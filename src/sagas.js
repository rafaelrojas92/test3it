import { takeEvery, takeLatest } from 'redux-saga/effects'

import init from './init.saga'
import Base from './Base/actions/base.action'

export default function* sagas() {
  yield takeEvery('INITIAL_LOADING', init)
  yield takeEvery('BASE_GET_DETAIL', Base.getMainDetail)
  yield takeEvery('BASE_GET_INFO_DETAIL', Base.getInfoDetail)
}
