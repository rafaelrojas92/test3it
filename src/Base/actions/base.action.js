import axios from 'axios'
import { call, put } from 'redux-saga/effects'

import { formatThousandsSeparators } from '../../utils'

export default {
  *getMainDetail({ payload }) {
    try {
      yield put({ type: 'SHOW_LOADING' })
      const { data, navigation } = payload

      const { data: dataResponse} = yield call(axios.get, `https://mindicador.cl/api/${data.codigo}`)

      const serieFormatted = dataResponse.serie.map( el => {
        if(dataResponse.unidad_medida !== 'Porcentaje'){
          const splitValor = el.valor.toString().split('.')
          el.valor = `${formatThousandsSeparators(splitValor[0])},${splitValor[1] ? splitValor[1] : '00'}`
        }
        return el
      })

      yield put({ type: 'SET_MAIN_DETAIL', payload: serieFormatted})
      return navigation.navigate('MainDetail', { headerTitle: data.nombre, unidadMedida: data.unidad_medida})
    } catch (error) {
      yield put({ type: 'SHOW_ERROR' })
    } finally {
      yield put({ type: 'HIDE_LOADING' })
    }
  },
  *getInfoDetail({ payload }) {
    try {
      yield put({ type: 'SHOW_LOADING' })
      const { data, navigation } = payload

      const { data: dataResponse } = yield call(axios.get, `https://mindicador.cl/api/${data.codigo}`)
      
      const sortedDates = dataResponse.serie.sort((a,b)=>{
        const dateA = new Date(a.fecha)
        const dateB = new Date(b.fecha)        
        return dateB - dateA
      })
      
      yield put({ type: 'SET_INFO_MOST_RECENT', payload: sortedDates[0]})

      const firstTen = sortedDates.slice(0, Math.min(10,sortedDates.length))

      yield put({ type: 'SET_INFO_DETAIL', payload: firstTen})
      
      return navigation.navigate('InfoContainer', { headerTitle: data.nombre, unidadMedida: data.unidad_medida})
    } catch (error) {
      yield put({ type: 'SHOW_ERROR' })
    } finally {
      yield put({ type: 'HIDE_LOADING' })
    }
  },
}
