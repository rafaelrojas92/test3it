import _ from 'lodash'
import { connect } from 'react-redux'
import React, { useEffect, useState } from 'react'
import { View, PermissionsAndroid, ScrollView, StyleSheet, } from 'react-native'

import Header from '../components/Header'
import { normalizeSize, COLORS } from '../constants'
import ButtonIndicador from '../components/ButtonIndicador'


const BaseContainer = ({ listaIndicadoresMain, init, getMainDetail, getInfoDetail }) => {
  useEffect(() => {
    init()
  }, []);

  const [lastSeen, setLastSeen] = useState(null);

  const [unidadMedida, setUnidadMedida] = useState(null);

  const requestLocationPermission = async (data) => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        setLastSeen(data.codigo)
        setUnidadMedida(data.unidad_medida)
        getMainDetail({ data })
      } else {
        alert("Activa el permiso de ubicación para utilizar esta función");
      }
    } catch (err) {
      alert("Se produjo un error, favor reintente");
    }
  }

  return (
    <View style={styles.mainContainer}>
      <Header title='Indicadores' />
      <View style={styles.contentContainer}>
        <ScrollView style={styles.scrollViewContainer}>
          {_.map(listaIndicadoresMain, (data, key) => {
            if (key > 2) {
              return (
                <ButtonIndicador
                  data={data}
                  key={key}
                  onPressMain={() => requestLocationPermission(data)}
                  onPressInfo={() => getInfoDetail({ data })}
                  isLastSeen={lastSeen === data.codigo}
                  unidadMedidaVista={unidadMedida === data.unidad_medida} />
              )
            }
          })}
        </ScrollView>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  titleContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  contentContainer: {
    flex: 7,
    marginTop: normalizeSize(15),
    paddingHorizontal: normalizeSize(15),
  },
  scrollViewContainer: { 
    flex: 1, 
  },
})

const mapStateToProps = state => {
  const {
    Base: {
      listaIndicadoresMain
    },
  } = state

  return {
    listaIndicadoresMain
  }
}
const mapDispatchToProps = (dispatch, { navigation }) => ({
  init: () => dispatch({ type: 'INITIAL_LOADING', payload: { navigation } }),
  getMainDetail: (payload) => dispatch({ type: 'BASE_GET_DETAIL', payload: { ...payload, navigation } }),
  getInfoDetail: (payload) => dispatch({ type: 'BASE_GET_INFO_DETAIL', payload: { ...payload, navigation } }),
})

export default connect(mapStateToProps, mapDispatchToProps)(BaseContainer)