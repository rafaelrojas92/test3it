const initialState = {
  loading: false,
  listaIndicadoresMain: [],
  mainDetail: [],
  infoDetail: [],
  infoMostRecent: {}
}

export default function Base(state = initialState, action) {
  switch (action.type) {
    case 'SHOW_LOADING':
      return {
        ...state,
        loading: true,
      }
    case 'HIDE_LOADING':
      return {
        ...state,
        loading: false,
      }
    case 'SET_LISTA_INDICADORES_MAIN':
      return {
        ...state,
        listaIndicadoresMain: action.payload,
      }
    case 'SET_MAIN_DETAIL':
      return {
        ...state,
        mainDetail: action.payload,
      }
    case 'SET_INFO_DETAIL':
      return {
        ...state,
        infoDetail: action.payload,
      }
    case 'SET_INFO_MOST_RECENT':
      return {
        ...state,
        infoMostRecent: action.payload,
      }
    case 'LOGOUT':
      return initialState
    default:
      return state
  }
}
