import _ from 'lodash'
import React from 'react'
import { connect } from 'react-redux'
import { View, Text, ScrollView, StyleSheet, } from 'react-native'

import Header from '../../components/Header'
import { COLORS, normalizeSize } from '../../constants'

const MainDetail = ({ route, mainDetail }) => {

  const unidadMedida = route.params.unidadMedida

  return (
    <View style={styles.mainContainer}>
      <Header title={route.params.headerTitle} withGoBack />
      <View style={styles.contentContainer}>
        <View style={styles.detailContainerTitle}>
          <Text style={styles.textTitles}>Fecha</Text>
          <Text style={styles.textTitles}>{unidadMedida}</Text>
        </View>
        <ScrollView style={styles.scrollViewContainer}>
          {_.map(mainDetail, (data, key) => (
            <View key={key} style={styles.detailContainer}>
              <Text style={styles.textDetail}>{data.fecha.split('T')[0]}</Text>
              <Text style={styles.textDetail}>
                {unidadMedida !== 'Porcentaje' && '$'}
                {data.valor}
                {unidadMedida === 'Porcentaje' && '%'}
              </Text>
            </View>
          ))}
        </ScrollView>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  contentContainer: {
    flex: 7,
    paddingHorizontal: normalizeSize(15),
  },
  detailContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: normalizeSize(45),
    paddingLeft: normalizeSize(10),
    borderRadius: normalizeSize(12),
    marginVertical: normalizeSize(15),
    backgroundColor: COLORS.background,
  },
  detailContainerTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    height: normalizeSize(40),
    marginTop: normalizeSize(15),
    paddingLeft: normalizeSize(10),
    borderRadius: normalizeSize(12),
  },
  textTitles: {
    flex: 1,
  },
  scrollViewContainer: {
    flex: 1,
  },
  textDetail: {
    flex: 1,
  },
})

const mapStateToProps = state => {
  const {
    Base: {
      mainDetail
    },
  } = state

  return {
    mainDetail
  }
}
const mapDispatchToProps = (dispatch, { navigation }) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(MainDetail)