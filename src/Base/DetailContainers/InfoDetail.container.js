import _ from 'lodash'
import React from 'react'
import { connect } from 'react-redux'
import { LineChart } from 'react-native-chart-kit'
import { View, Text, Dimensions, StyleSheet, } from 'react-native'

import Header from '../../components/Header'
import { formatThousandsSeparators } from '../../utils'
import { normalizeSize, COLORS, SIZES } from '../../constants'

const InfoDetail = ({ route, infoDetail, infoMostRecent }) => {

  const unidadMedida = route.params.unidadMedida

  const chartConfig = {
    backgroundGradientFrom: COLORS.black,
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
    strokeWidth: normalizeSize(2),
  };

  const labels = infoDetail.map(el => {
    return el.fecha.split('T')[0]
  }).reverse()

  const dataGraph = infoDetail.map(el => {
    return el.valor
  }).reverse()

  const data = {
    labels: labels,
    datasets: [
      {
        data: dataGraph,
      }
    ],
  }

  const screenWidth = Dimensions.get("window").width * 0.9

  const valorFormatted = () => {
    const splitValor = infoMostRecent.valor.toString().split('.')
    return `${formatThousandsSeparators(splitValor[0])},${splitValor[1] ? splitValor[1] : '00'}`
  }

  return (
    <View style={styles.mainContainer}>
      <Header title={route.params.headerTitle} withGoBack />
      <View style={styles.contentContainer}>
        <View style={styles.detailContainerTitle}>
          <Text style={styles.titleText}>
            {unidadMedida !== 'Porcentaje' && '$'}
            {unidadMedida === 'Porcentaje' ? infoMostRecent.valor : valorFormatted()}
            {unidadMedida === 'Porcentaje' && '%'}
          </Text>
          <View style={styles.nameContainer}>
            <Text style={styles.textDescription}>
              Nombre
            </Text>
            <Text style={styles.nameText}>{route.params.headerTitle}</Text>
          </View>
          <View style={styles.textContainerDateUA}>
            <Text style={styles.textDescription}>
              Fecha
            </Text>
            <Text style={styles.dateText}>{infoMostRecent.fecha.split('T')[0]}</Text>
          </View>
          <View style={styles.textContainerDateUA}>
            <Text style={styles.textDescription}>
              Unidad Medida
            </Text>
            <Text style={styles.dateUAText}>{unidadMedida}</Text>
          </View>
        </View>
        <View style={styles.chartContainer}>
          <LineChart
            data={data}
            width={screenWidth}
            height={normalizeSize(290)}
            chartConfig={chartConfig}
            verticalLabelRotation={90}
            horizontalLabelRotation={-45}
            style={styles.chartStyle}
            yAxisLabel={unidadMedida !== 'Porcentaje' ? '$' : undefined}
            yAxisSuffix={unidadMedida === 'Porcentaje' ? '%' : undefined}
          />
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  contentContainer: {
    flex: 7,
    alignItems: 'center',
    paddingHorizontal: normalizeSize(15),
  },
  titleText: {
    fontSize: SIZES.h1,
    fontWeight: 'bold',
  },
  textDescription: {
    flex: 1
  },
  nameContainer: {
    flexDirection: 'row',
    marginTop: normalizeSize(25),
    marginVertical: normalizeSize(5),
    borderBottomWidth: normalizeSize(1),
  },
  nameText: {
    flex: 2,
    textAlign: 'right'
  },
  textContainerDateUA: {
    flexDirection: 'row',
    marginVertical: normalizeSize(5),
    borderBottomWidth: normalizeSize(1),
  },
  dateUAText: {
    flex: 1,
    textAlign: 'right'
  },
  detailContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: normalizeSize(35),
    paddingLeft: normalizeSize(10),
    borderRadius: normalizeSize(12),
    marginVertical: normalizeSize(10),
    backgroundColor: COLORS.background,
  },
  detailContainerTitle: {
    flex: 1.5,
    width: '90%',
    alignItems: 'center',
    marginTop: normalizeSize(15),
    paddingLeft: normalizeSize(10),
    borderRadius: normalizeSize(12),
  },
  chartContainer: {
    flex: 3,
    backgroundColor: COLORS.black,
    marginBottom: normalizeSize(40),
    borderRadius: normalizeSize(12),
  },
  chartStyle: {
    borderRadius: normalizeSize(12),
    paddingBottom: normalizeSize(45),
  }
})

const mapStateToProps = state => {
  const {
    Base: {
      infoDetail,
      infoMostRecent,
    },
  } = state

  return {
    infoDetail,
    infoMostRecent,
  }
}
const mapDispatchToProps = (dispatch, { navigation }) => ({})

export default connect(mapStateToProps, mapDispatchToProps)(InfoDetail)