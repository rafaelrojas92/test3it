
const formatThousandsSeparators = (word = '') => {
    const formatePuntos = word
      .toString()
      .replace(/\./g, '')
      .replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    const formateThousandsSeparators = formatePuntos
    return formateThousandsSeparators
  }

  export {
      formatThousandsSeparators
  }
  