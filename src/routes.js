
import BaseContainer from './Base/Base.container'
import MainDetailContainer from './Base/DetailContainers/MainDetail.container'
import InfoDetailContainer from './Base/DetailContainers/InfoDetail.container'

export const BaseScreens = {
  Base: BaseContainer,
  MainDetail: MainDetailContainer,
  InfoContainer: InfoDetailContainer,
}
