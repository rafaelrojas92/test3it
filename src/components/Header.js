import React from 'react'
import AntDesignIcon from 'react-native-vector-icons/AntDesign'
import { useNavigation } from '@react-navigation/native'

import { COLORS, SIZES, normalizeSize } from '../constants'
import { View, Text, StyleSheet, TouchableHighlight } from 'react-native'

const Header = ({ title, withGoBack }) => {

  const navigation = useNavigation()

  return (
    <View style={styles.titleContainer}>
      {withGoBack &&
        <TouchableHighlight
          underlayColor={COLORS.primary}
          style={styles.goBackContainer}
          onPress={() => navigation.goBack()}
        >
          <AntDesignIcon
              name='left'
              color={COLORS.white}
              size={normalizeSize(30)}
            />
        </TouchableHighlight>
      }
      <View style={styles.titleContainerText}>
        <Text style={styles.textTitle}>{title}</Text>
      </View>

    </View>
  )
}

const styles = StyleSheet.create({
  titleContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: COLORS.primary,
    paddingHorizontal: normalizeSize(15),
  },
  textTitle: {
    fontSize: SIZES.h1,
    fontWeight: 'bold',
    color: COLORS.white,
  },
  goBackContainer: { 
    flex: 1,
  },
  titleContainerText: {
    flex: 7, 
    alignItems: 'flex-start',
  },
})

export default Header