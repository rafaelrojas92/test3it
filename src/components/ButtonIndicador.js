import React from 'react'
import AntDesignIcon from 'react-native-vector-icons/AntDesign'
import { View, Text,  StyleSheet, TouchableHighlight } from 'react-native'

import { COLORS, SIZES, normalizeSize } from '../constants';

const ButtonIndicador = ({ data, onPressMain, onPressInfo, isLastSeen, unidadMedidaVista }) => {
  
  return (
    <TouchableHighlight
      underlayColor={COLORS.white}
      onPress={onPressMain}
      style={[styles.mainButtonsContainer, isLastSeen && styles.borderSelected]}
    >
      <>
        <View style={styles.textContainer}>
          <Text style={styles.textName}>
            {data.nombre}
          </Text>
          <Text style={[styles.textType, unidadMedidaVista && styles.unidadMedidaSelected]}>
            {data.unidad_medida}
          </Text>
        </View>
        <View style={styles.infoContainerButton}>
          <TouchableHighlight
            onPress={onPressInfo}
            underlayColor='#d9d9d9'
            style={styles.touchableInfo}
          >
            <AntDesignIcon
              name='infocirlceo'
              color={COLORS.primary}
              size={normalizeSize(35)}
            />

          </TouchableHighlight>
          <AntDesignIcon
            name='right'
            color={COLORS.primary}
            size={normalizeSize(25)}
          />
        </View>
      </>
    </TouchableHighlight>
  )
};



const styles = StyleSheet.create({
  mainButtonsContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    height: normalizeSize(65),
    borderRadius: normalizeSize(12),
    marginVertical: normalizeSize(17),
    backgroundColor: COLORS.background,
  },
  textContainer: { 
    flex: 4 
  },
  borderSelected: {
    borderColor: COLORS.primary,
    borderWidth: normalizeSize(2), 
  },
  textName: {
    color: COLORS.black,
    fontSize: SIZES.body1,
    paddingLeft: normalizeSize(15),
    paddingVertical: normalizeSize(5),
  },
  textType: {
    color: COLORS.gray,
    fontSize: SIZES.body1,
    paddingLeft: normalizeSize(15),
    paddingVertical: normalizeSize(2),
  },
  unidadMedidaSelected: {
    fontWeight: 'bold',
    color: COLORS.primary, 
  },
  infoContainerButton: { 
    flex: 1, 
    flexDirection: 'row', 
    alignItems: 'center', 
    paddingRight: normalizeSize(10) 
  },
  touchableInfo: { 
    flex: 1, 
    padding: normalizeSize(5) 
  },
})

export default ButtonIndicador