# test3IT

Aplicación para ver indicadores económicos entregados por Mindicador (https://mindicador.cl),
donde se entrega el detalle histórico de cada indicador, y una vista gráfica de los últimos
10 valores del indicador.

Al seleccionar un indicador, este queda marcado con un borde azul hasta que se seleccione
otro indicador. Además, se destaca la unidad de medida en color azul de todos los indicadores 
que tengan la misma unidad de medida que el indicador seleccionado.

Se solicita el permiso de ubicación para acceder a la opción de ver el detalle de cada
indicador, pero no así del detalle del gráfico. Si se rechaza el permiso, dicha función
queda bloqueada.

# Instalación

npm install

# Uso

react-native run-android