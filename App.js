/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import { Provider } from 'react-redux'
 import React, { Component } from 'react'
 import { SafeAreaProvider } from 'react-native-safe-area-context'
 import { StyleSheet, StatusBar, Platform, Linking } from 'react-native'
 
 import {
   configureFonts,
   Provider as PaperProvider,
   DarkTheme as PaperDarkTheme,
   DefaultTheme as PaperDefaultTheme,
 } from 'react-native-paper'
 import {
   NavigationContainer,
   DarkTheme as NavigationDarkTheme,
   DefaultTheme as NavigationDefaultTheme,
 } from '@react-navigation/native'
 
 import store from './src/store'
 import NavigationContainerWithState from './src/navigation'
 
 
 export class App extends Component {
   state = {
     codePushUpdate: false,
     codePushStatus: 'Buscando actualización...',
     codePushPercentage: 0,
   }
 
   navigationRef = null
 
   getCurrentRoute = () =>
     this.navigationRef && this.navigationRef.getCurrentRoute()
 
 
 
   render() {
     const { codePushPercentage, codePushStatus, codePushUpdate } = this.state
 
     const linking = {
       prefixes: ['baseProject://'],
       async getInitialURL() {
         const url = await Linking.getInitialURL()
 
         if (url != null) {
           return url
         }
       },
       subscribe(listener) {
         const onReceiveURL = ({ url }) => listener(url)
 
         Linking.addEventListener('url', onReceiveURL)
 
         return () => {
           Linking.removeEventListener('url', onReceiveURL)
         }
       },
     }
 
     return (
       <Provider store={store}>
         <SafeAreaProvider>
           <PaperProvider>
             <StatusBar
               barStyle='light-content'
               hidden={Platform.OS === 'android'}
             />
             <NavigationContainer
               ref={ref => (this.navigationRef = ref)}
               linking={linking}
             >
               <NavigationContainerWithState
                 getCurrentRoute={this.getCurrentRoute}
               />
             </NavigationContainer>
           </PaperProvider>
         </SafeAreaProvider>
       </Provider>
     )
   }
 }
 
 const styles = StyleSheet.create()
 
 export default App
 